<?php
return [
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 'php2_simple_blog',
    'username'  => 'root',
    'password'  => '',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
];