<?php
namespace App\Repo;

use App\Model\Post;

class PostRepo extends BaseRepo {
    public $model = Post::class ;

    public function mostVisit($count)
    {
        return $this->model::orderBy('view_count', 'desc')->take($count)->get();
    }

}
